# Use an official Python runtime as a parent image
FROM python:3.7-stretch

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY ./requirements.txt /app/

RUN pip install --trusted-host pypi.python.org -r requirements.txt

COPY ./beatlex/__init__.py /app/beatlex/
COPY ./beatlex/mdtw/*.cpp ./beatlex/mdtw/*.hpp /app/beatlex/mdtw/
COPY ./setup.py ./README.md /app/

RUN python3 setup.py build_ext -if
