from distutils.core import setup, Extension
import numpy

setup(name="pybeatlex",
      version="0.1-dev",
      author="Laureline Nevin",
      author_email="laureline.nevin@inria.fr",
      url="https://gitlab.inria.fr/jnevin/pybeatlex",
      description="",
      long_description=open("./README.md", encoding="utf8").read(),
      include_dirs=[numpy.get_include()],
      packages=["beatlex"],
      requires=['numpy', 'tqdm'],
      ext_modules=[
          Extension('beatlex.mdtw', ['beatlex/mdtw/dtw.cpp', 'beatlex/mdtw/dtw_pystuff.cpp'],
                    extra_compile_args=["-std=c++11",
                                        "-pedantic-errors",
                                        "-Wno-unused-function",
                                        # Illegal to initialize PyMemberDef's char* members with string
                                        # litterals as per C++11
                                        "-Wno-write-strings"])
      ])
