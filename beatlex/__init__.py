from os import devnull
from typing import List, NamedTuple, Any, Tuple, Optional
import numpy as np
import tqdm

from . import mdtw


class BeatLex:
    """Implementation of the BeatLex (Beat Lexicon) timeseries summarization algorithm, as described in
    "BEATLEX: Summarizing and Forecasting Time Series with Patterns.", Hooi12, Bryan, et al.
    """

    def __init__(self, s_max: int, k_max: int, dtw_w: int, new_vocab_thresh: float = 0.4, s_min=None, progbar=None,
                 verbose: bool = False):
        """Creates a new BeatLex model with empty vocabulary

        :param s_max: Maximum term size to consider adding to the vocabulary
        :param k_max: Maximum vocabulary size to learn
        :param dtw_w: Width of the Sakoe-Chiba band for DTW computation.
                0 to disable the band and compute DTW on the full matrix
        :param new_vocab_thresh: New vocabulary term creation threshold
        :param progbar: Display progress bars (via TQDM). True/False to enable/disable or pass any of the tqdm.tqdm_*
                (e.g tqdm_notebook) to use a custom one
        :param verbose: Print progress update to the standard output
        """

        if s_min is not None and s_min < 1:
            raise AttributeError("s_min must be strictly positive")
        self._s_max = s_max
        self._s_min = s_max // 10 if s_min is None else s_min
        self._k_max = k_max
        self._dtw_w = dtw_w
        self._vocab: List[np.ndarray] = []
        self._vocabcount: List[int] = []
        self._new_vocab_thresh = new_vocab_thresh
        self._verbose = verbose is True

        if progbar or (progbar is None and not verbose):
            if callable(progbar):
                self._tqdm = progbar
            else:
                self._tqdm = tqdm.tqdm
        else:
            # Blackhole the TQDM output
            nullout = open(devnull, "w")
            self._tqdm = lambda *args, **kwargs: tqdm.tqdm(*args, **kwargs, file=nullout)

    class Segmentation(NamedTuple):
        """Assignment of a vocabulary term with index `id` to the subseries `[beginning:beginning+length]`
        of the data"""
        id: int
        beginning: int
        length: int

    def fit(self, X: Any) -> List[Segmentation]:
        """Alias for fit_transform(build_vocab=True)"""
        return self.fit_transform(X, build_vocab=True)

    def transform(self, X: Any) -> List[Segmentation]:
        """Alias for fit_transform(build_vocab=False)"""
        return self.fit_transform(X, build_vocab=False)

    def fit_transform(self, X: Any, build_vocab: bool = True) -> List[Segmentation]:
        """Computes BeatLex vocabulary and segmentation for input data X

        :param X: Numpy array-like object containing the timeseries to build vocabulary from and segmentize/assign
        :param build_vocab: If true, add new terms to the learned vocabulary
        :return: segmentation
        """

        try:
            dat: np.ndarray = np.asarray(X, dtype=np.float64)
            del X
        except TypeError:
            raise TypeError("First argument must be a numpy array-like of float-like objects")

        assert np.isfinite(dat).all(), "Input array contains non-finite values"

        if not build_vocab and not self._vocab:
            raise RuntimeError("fit_transformed called with build_vocab=False and no precomputed vocab")
        if len(dat.shape) > 2:
            raise RuntimeError("input X must be one- or two-dimensional")
        if len(dat.shape) == 1:
            dat = dat.reshape((1, dat.shape[0]))
        # Cf = size of a float in bits. To avoid accounting for the overhead which is not part of the datatype's size
        # per se, we take the size of a float as an element of a numpy array
        # c_f = np.zeros(1, dtype=dat.dtype).nbytes * 8  # Turns out they don't actually use it in their code
        new_cluster_threshold = self._new_vocab_thresh * dat.var()

        segmentation: List[BeatLex.Segmentation] = []
        i = 0
        mdtw.preallocate_matrix(self._s_max + self._dtw_w, self._s_max, dat.shape[0])
        with self._tqdm(total=dat.shape[1], desc="Processed data") as bar:
            while i < dat.shape[1]:
                nextdat = dat[:, i:]
                match_id, match = self._best_vocab_prefix_match(nextdat)
                assert match or not self._vocab
                avg_cost = match.get_total_cost() / (match.match_length * dat.shape[0]) if match else None
                if self._verbose and match:
                    print("Segment %d : best match=%d, cost=%f, match_len=%d, avg_cost=%f (threshold = %f)" %
                          (len(segmentation), match_id, match.get_total_cost(), match.match_length, avg_cost,
                           new_cluster_threshold))
                    if len(segmentation) == 9:
                        np.save("./costmat.npy", mdtw.get_matrix())
                if match_id is not None and \
                        (avg_cost < new_cluster_threshold or dat.shape[1] - i < self._s_max // 4 or
                         len(self._vocab) >= self._k_max or not build_vocab):
                    # Assign best matching word to prefix
                    self._vocab_merge(dat[:, i: i + match.match_length], match_id, match)
                    seg = self.Segmentation(match_id, i, match.match_length)
                    if self._verbose is True:
                        print("Mached to vocabulary term %d" % seg.id)
                elif build_vocab and len(self._vocab) < self._k_max:
                    # Add prefix as new vocabulary term
                    new_term = self._new_vocab_term(nextdat)
                    seg = self.Segmentation(new_term, i, self._vocab[new_term].shape[1])
                    if self._verbose is True:
                        print("Added as new term %d of length %d" % (new_term, self._vocab[new_term].shape[1]))
                else:
                    raise RuntimeError("Can't assign term from empty vocabulary")

                assert seg.length > 0
                segmentation.append(seg)
                i += seg.length
                bar.update(seg.length)
                if self._verbose is True:
                    print("Processed prefix of size %d, %d/%d remaining" % (seg.length, dat.shape[1] - i, dat.shape[1]))

        return segmentation

    def _best_vocab_prefix_match(self, X: np.ndarray, extra_term: Optional[np.ndarray] = None,
                                 return_match: bool = True) -> Tuple[Optional[int], Optional[mdtw.DTWMatch]]:
        """Finds the vocabulary term that best matches the beginning of the given timeseries

        :param X: Timeseries to match terms against
        :param extra_term: Optional "extra term" to consider as part the 'k+1'th term of the vocabulary,
            as needed by _new_vocab_term
        :param return_match: Return the DTWMatch object alongside the match_id. Otherwise, returns the total cost
        :return: (matched vocab term ID, tuple of the match as returned by mdtw)
        """

        if not self._vocab and extra_term is None:
            return None, None

        vocab = self._vocab
        if extra_term is not None:
            vocab = vocab + [extra_term]

        def tq():
            return self._tqdm(enumerate(vocab), desc="best vocab prefix match", unit="term", leave=False) \
                if extra_term is None else enumerate(vocab)

        prefix = X[:, :self._s_max + self._dtw_w]
        matches = [(i, mdtw.dtw(prefix, v, scband_width=self._dtw_w,
                                min_match=min(prefix.shape[1], v.shape[1] // 2,
                                              max(1, v.shape[1] - self._dtw_w)),
                                return_total_cost=not return_match)) for i, v in tq()]
        match_id, match = min(matches, key=lambda x: x[1].get_total_cost() if return_match else x[1])

        return match_id, match

    def _vocab_merge(self, x: np.ndarray, term_id: int, match: mdtw.DTWMatch) -> None:
        """VocabMerge: update matched term with new data"""
        aligned = match.align_series(x)
        assert aligned.shape == self._vocab[term_id].shape
        self._vocab[term_id] = np.average([aligned, self._vocab[term_id]], axis=0,
                                          weights=[self._vocabcount[term_id], 1])
        self._vocabcount[term_id] += 1

    def _new_vocab_term(self, X) -> int:
        """NewVocabLength: calculate optimal prefix length from data for a new vocabulary term"""
        length_costs = []
        s_min = self._s_min
        s_max = self._s_max
        with self._tqdm(total=((s_max * (s_max + 1) - s_min * (s_min + 1)) // 2), desc="new vocab term length",
                        leave=False) as bar:
            for i in range(s_min, min(self._s_max+1, X.shape[1])):
                length_costs.append((i, self._best_vocab_prefix_match(X[:, i:], extra_term=X[:, :i],
                                                                      return_match=False)))
                bar.update(i)
        # Reverse the first half of the array: in case of null variance in the input array, resulting in all costs being
        # equal, this favours a match length of s_max / 2
        length_costs[:len(length_costs)//2] = length_costs[:len(length_costs)//2][::-1]
        best_length, best_match = min(length_costs, key=lambda x: x[1][1] / x[0])
        if self._verbose:
            print("Best length: %d (next best prefix match : %d for cost %f)" %
                  (best_length, best_match[0], best_match[1]))
        term = X[:, :best_length]
        self._vocab.append(term)
        self._vocabcount.append(1)
        return len(self._vocab) - 1  # ID of the new term

    @property
    def vocab(self):
        return self._vocab[:]
