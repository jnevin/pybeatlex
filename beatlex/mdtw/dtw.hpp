#ifndef MDTW_H
#define MDTW_H
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>

#include "numpy/arrayobject.h"

PyMODINIT_FUNC PyInit_mdtw();

namespace mdtw {
    // Internal dtype used by mdtw. If you wish to change the type make surey ou change all relevant values accordingly
    // in the following lines
	using costmat_item_t = npy_float64;
	static const int COST_MAT_TYPENUM = NPY_FLOAT64;
	static const costmat_item_t COST_MAT_INF = INFINITY;

/* DTW struct definitions */
	typedef struct path_node {
		size_t x, y;
		struct path_node *next;
	} path_node;

	typedef struct {
		PyObject_HEAD
		size_t nvars;
		PyObject *costs;
		PyObject *pypaths;
		path_node **path;
		PyObject *length;
	} DTWMatch;

	struct module_state {
		PyObject *error;
		PyArray_Descr *dtype;
		costmat_item_t *preinit_array;
		size_t preinit_array_len, last_width, last_height, last_nvars;
	};

	inline struct module_state* get_state(PyObject* m) {
		return static_cast<struct module_state*>(PyModule_GetState(m));
	}

/* dtw methods declaration */

	PyObject *mdtw_dtw(PyObject *self, PyObject *args, PyObject *kwargs);

	PyObject *preallocate_matrix(PyObject *self, PyObject *args);

	PyObject *deallocate_matrix(PyObject *self);

	PyObject *get_matrix(PyObject *self);

	PyObject *get_matrix_size(PyObject *self);

	void _dtw(costmat_item_t *matrix, costmat_item_t *xdat, costmat_item_t *ydat, size_t xstride, size_t ystride, long w, long h,
		          long min_match, long bwidth, costmat_item_t warp_cost_x, costmat_item_t warp_cost_y);

	path_node *_dtw_backtrace(costmat_item_t *matrix, long h, long match_len, costmat_item_t warp_cost_x,
	                          costmat_item_t warp_cost_y);

	namespace match {
		extern PyTypeObject DTWMatchType;

		PyObject *get_path(DTWMatch *self);

		PyObject *align_series(DTWMatch *self, PyObject *args);

		PyObject *get_total_cost(DTWMatch *self);
	}
}
#endif