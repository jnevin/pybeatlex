#include <sstream>
#include <iostream>
#include "dtw.hpp"
#include "cpppyutils.hpp"
#include "structmember.h"

// Translate i, j coordinates to the inline index in the full cost matrix
#define MAT_INLINE(v, i, j, w, h) ((v) * (h) * (w) + (i) * (h) + (j))
#define FULLMAT_INLINE(v, i, j) MAT_INLINE(v, i, j, w + 1, h + 1)
#define CHECK_OR_RAISE(cond, ...) if (!(cond)) {\
    PyErr_SetObject(PyExc_AttributeError, PyUnicode_FromFormat(__VA_ARGS__));\
    return nullptr;\
}
#define SNPRINTF(varname, length, format, ...) char varname[length];\
    PyOS_snprintf(varname, length, format, __VA_ARGS__)

using namespace pyutils;

namespace mdtw {
	static const char *const preallocate_matrix_docstring = nullptr; // TODO docstring

	bool allocate_matrix(struct module_state *st, size_t size) {
		if (size > st->preinit_array_len)
			try {
				std::cout << "Allocating / resizing matrix to size "<< size << std::endl;
				delete[] st->preinit_array;
				st->preinit_array = new costmat_item_t[size];
				memset(st->preinit_array, 0, sizeof(costmat_item_t) * size);
				st->preinit_array_len = size;
			} catch (std::bad_alloc &ex) {
				st->preinit_array_len = 0;
				PyErr_SetString(PyExc_MemoryError, ex.what());
				return false;
			}
		return true;
	}

	PyObject *preallocate_matrix(PyObject *self, PyObject *args) {
		struct module_state *st = get_state(self);
		unsigned long size = 0, height = 0, nvars = 1;
		if (!PyArg_ParseTuple(args, "k|kk", &size, &height, &nvars)) return nullptr;
		CHECK_OR_RAISE(size > 1,
		               "Allocated size must be non-null. Use deallocate_matrix() to free the preallocated cost matrix");
		CHECK_OR_RAISE(nvars > 0, "nvars cannot be null")
		if (height > 0) {
			size = (size + 1) * (height + 1) * nvars;
		}

		if (!allocate_matrix(st, size))
			return nullptr;

		if (height > 0) {
			st->last_width = size;
			st->last_height = height;
			st->last_nvars = nvars;
		}

		Py_RETURN_NONE;
	}

	static const char *const deallocate_matrix_docstring = nullptr; // TODO docstring

	PyObject *deallocate_matrix(PyObject *self) {
		struct module_state *st = get_state(self);
		if (st->preinit_array != nullptr) {
			delete[] st->preinit_array;
			st->preinit_array = nullptr;
			st->preinit_array_len = 0;
			Py_RETURN_TRUE;
		}
		Py_RETURN_FALSE;
	}


	static const char *const get_matrix_docstring = nullptr; // TODO docstring

	PyObject *get_matrix(PyObject *self) {
		struct module_state *st = get_state(self);
		CHECK_OR_RAISE(st->preinit_array != nullptr, "There is no preallocated matrix")

		if (st->preinit_array_len < (st->last_nvars + 1) * (st->last_width + 1)) {
			PyErr_SetString(PyExc_AttributeError, "Preallocated array is too small for given dimensions");
			return nullptr;
		}

		bool multivar = st->last_nvars > 1;
		auto w = st->last_width, h = st->last_height;
		npy_intp shape[3] = {(npy_intp) st->last_nvars, (npy_intp) w, (npy_intp) h};
		PyObject *array = PyArray_SimpleNew(multivar ? 3 : 2, multivar ? shape : shape + 1, COST_MAT_TYPENUM);
		if (!PyArray_Check(array))
			return nullptr;

		auto data = static_cast<costmat_item_t *>(PyArray_DATA(reinterpret_cast<PyArrayObject *>(array)));

		for (size_t v = 0; v < st->last_nvars; v++)
			for (size_t i = 0; i < st->last_width; i++) {
				memcpy(&data[v * w * h + i * h],
				       &st->preinit_array[v * (h + 1) * (h + 1) + (i + 1) * (h + 1) + 1],
				       sizeof(costmat_item_t) * h);
			}

		return array;
	}

	static const char *const get_matrix_size_docstring = nullptr; // TODO docstring

	PyObject *get_matrix_size(PyObject *self) {
		struct module_state *st = get_state(self);
		return PyLong_FromSize_t(st->preinit_array_len);
	}

	static const char *const dtw_docstring = nullptr; // TODO docstring

	PyObject *mdtw_dtw(PyObject *self, PyObject *args, PyObject *kwargs) {
		struct module_state *st = get_state(self);

		static const char *kwlist[] = {"x", "y", "scband_width", "min_match", "warp_penalty", "return_total_cost", nullptr};
		PyObject *xarg = nullptr, *yarg = nullptr;

		long bwidth = 0, min_match = 0, warp_penalty = 1, return_cost = 0;
		if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO|kkkk", const_cast<char **>(kwlist),
		                                 &xarg, &yarg, &bwidth, &min_match, &warp_penalty, &return_cost))
			return nullptr;

		CHECK_OR_RAISE(PyArray_Check(xarg) && PyArray_Check(yarg),
		               "xarg and yarg arguments to dtw must be numpy arrays")
		auto *x = (PyArrayObject *) xarg, *y = (PyArrayObject *) yarg;
		if (PyArray_TYPE(x) != COST_MAT_TYPENUM)
			x = (PyArrayObject *) PyArray_Cast(x, COST_MAT_TYPENUM);
		if (PyArray_TYPE(y) != COST_MAT_TYPENUM)
			y = (PyArrayObject *) PyArray_Cast(y, COST_MAT_TYPENUM);

		CHECK_OR_RAISE(PyArray_NDIM(x) <= 2 && PyArray_NDIM(y) <= 2, "input arrays must be mono- or bi-dimensionnal")
		CHECK_OR_RAISE(PyArray_NDIM(x) == PyArray_NDIM(y), "input arrays must have the same dimensionality")
		CHECK_OR_RAISE(PyArray_NDIM(x) == 1 || PyArray_DIM(x, 0) == PyArray_DIM(y, 0),
		               "Multidimensionnal input arrays must be of the same length in the first dimension")
		CHECK_OR_RAISE(PyArray_CanCastArrayTo(x, st->dtype, NPY_SAFE_CASTING) &&
		               PyArray_CanCastArrayTo(y, st->dtype, NPY_SAFE_CASTING),
		               "Input arrays be of a dtype that is safely castable to %s", st->dtype->typeobj->tp_name);

		if (PyArray_DESCR(x)->type_num != COST_MAT_TYPENUM)
			x = reinterpret_cast<PyArrayObject *>(PyArray_CastToType(x, st->dtype, 0));

		if (PyArray_DESCR(y)->type_num != COST_MAT_TYPENUM)
			y = reinterpret_cast<PyArrayObject *>(PyArray_CastToType(y, st->dtype, 0));

		if (x == nullptr || y == nullptr)
			return nullptr;

		if ((PyObject *) x != xarg || (PyObject *) y != yarg) {
			SNPRINTF(msg, 256, "Casting input arrays to internal dtype. "
			                   "For best performances, use arrays of same dtype (%s)",
			         st->dtype->typeobj->tp_name);
			PyErr_WarnEx(PyExc_RuntimeWarning, msg, 2);
		}

		long w = (long) PyArray_DIM(x, PyArray_NDIM(x) - 1),
				h = (long) PyArray_DIM(y, PyArray_NDIM(x) - 1),
				n = PyArray_NDIM(x) > 1 ? (long) PyArray_DIM(x, 0) : 1L,
//				size = w * h,
				need_size = (w + 1) * (h + 1) * n;

		st->last_width = static_cast<size_t>(w);
		st->last_height = static_cast<size_t>(h);
		st->last_nvars = static_cast<size_t>(n);

		CHECK_OR_RAISE(bwidth >= 0, "SC band width can't be negative")
		CHECK_OR_RAISE(min_match >= 0, "Minimum match length can't be negative")
		CHECK_OR_RAISE(min_match <= w, "Given minimum match length is superior to the size of the first array")

		// Ensure the preallocated matrix is large enough
		allocate_matrix(st, static_cast<size_t>(need_size));
		costmat_item_t *matrix;
		matrix = st->preinit_array;


		// Cost of X- and Y-warping, as defined by the BeatLex algorithm. Not specified in the article:
		// Warp costs are the log of the length of the input series *multiplied by their standard deviation*
		costmat_item_t warp_cost_x = warp_penalty ? (costmat_item_t) (log2f(w) * PyFloat_AsDouble(
				PyArray_Std(x, NPY_MAXDIMS, COST_MAT_TYPENUM, nullptr, 0))) : 0.f,
				warp_cost_y = warp_penalty ? (costmat_item_t) (log2f(h) * PyFloat_AsDouble(
				PyArray_Std(y, NPY_MAXDIMS, COST_MAT_TYPENUM, nullptr, 0))) : 0.f;

		auto *xdat = reinterpret_cast<costmat_item_t *>(PyArray_DATA(x)),
				*ydat = reinterpret_cast<costmat_item_t *>(PyArray_DATA(y));
		auto stride_dim = PyArray_NDIM(x) > 1 ? 1 : 0;
		auto xstride = PyArray_STRIDE(x, stride_dim) / sizeof(costmat_item_t),
				xstride_var = stride_dim > 0 ? PyArray_STRIDE(x, 0) / sizeof(costmat_item_t) : 0,
				ystride = PyArray_STRIDE(y, stride_dim) / sizeof(costmat_item_t),
				ystride_var = stride_dim > 0 ? PyArray_STRIDE(y, 0) / sizeof(costmat_item_t) : 0;

		for (long i = 0; i < n; i++)
			_dtw(&matrix[i * (w + 1) * (h + 1)], &xdat[i * xstride_var], &ydat[i * ystride_var],
			     xstride, ystride, w, h, min_match, bwidth, warp_cost_x, warp_cost_y);
		if (PyErr_Occurred())
			return nullptr;

		long match_len;

		if (min_match > 0 && min_match < w) {
			costmat_item_t match_cost = INFINITY;
			match_len = 1;
			for (long l = min_match; l <= w; l++) {
				auto cost = matrix[FULLMAT_INLINE(0, l - 1, h - 1)];
				for (int i = 1; i < n; i++)
					cost += matrix[FULLMAT_INLINE(i, l - 1, h - 1)];
				if (cost * match_len < match_cost * l ||
				    (cost * match_len == match_cost * l && abs(l - h) < abs(match_len - h))) {
				    // in case of equal costs, favour match_len closest to h to get the most "diagonal" match possible
					match_len = l;
					match_cost = cost;
				}
			}
		} else {
			match_len = w;
		}

		PyObject *retval;

		if (return_cost) {
			costmat_item_t cost = 0.;
			for (int i = 0; i < n; i++) {
				cost += matrix[FULLMAT_INLINE(i, match_len - 1, h - 1)];
			}
			retval = PyFloat_FromDouble(cost);
		} else {
			auto *match = (DTWMatch *) PyObject_CallObject((PyObject *) &match::DTWMatchType, nullptr);
			match->nvars = (size_t) n;
			match->costs = PyList_New(n);
			for (int i = 0; i < n; i++) {
				auto cost = PyFloat_FromDouble(matrix[FULLMAT_INLINE(i, match_len - 1, h - 1)]);
				PyList_SetItem(match->costs, i, cost);
			}
			match->length = PyLong_FromLong(match_len);
			match->path = new path_node *[n];
			for (long i = 0; i < n; i++)
				match->path[i] = _dtw_backtrace(&matrix[i * (w + 1) * (h + 1)], h, match_len, warp_cost_x, warp_cost_y);
			if (PyErr_Occurred())
				return nullptr;

			retval = reinterpret_cast<PyObject *>(match);
		}

		if (matrix != st->preinit_array)
			delete[] matrix;

		if (x != (PyArrayObject *) xarg)
			Py_DECREF(x);
		if (y != (PyArrayObject *) yarg)
			Py_DECREF(y);

		return retval;
	}

/* dtw module metadata */
	static PyMethodDef mdtw_methods[] = {
			{"dtw",                (PyCFunction) mdtw_dtw,           METH_VARARGS | METH_KEYWORDS, dtw_docstring},
			{"preallocate_matrix", (PyCFunction) preallocate_matrix, METH_VARARGS, preallocate_matrix_docstring},
			{"deallocate_matrix",  (PyCFunction) deallocate_matrix,  METH_NOARGS,  deallocate_matrix_docstring},
			{"get_matrix",         (PyCFunction) get_matrix,         METH_NOARGS,  get_matrix_docstring},
			{"get_matrix_size",    (PyCFunction) get_matrix_size,    METH_NOARGS,  get_matrix_size_docstring},
			{nullptr,              nullptr, 0,                                     nullptr}
	};

	static int mdtw_traverse(PyObject *m, visitproc visit, void *arg) {
		Py_VISIT(get_state(m)->error);
		Py_VISIT(get_state(m)->dtype);
		return 0;
	}

	static int mdtw_clear(PyObject *m) {
		Py_CLEAR(get_state(m)->error);
		Py_CLEAR(get_state(m)->dtype);
		return 0;
	}

	static void mdtw_free(PyObject *m) {
		auto st = get_state(m);
		if (st == nullptr || st->preinit_array == nullptr)
			return;
		delete[] st->preinit_array;
	}

	static struct PyModuleDef moduledef = {
			PyModuleDef_HEAD_INIT,
			/* m_name */ "mdtw",
			/* m_doc */ nullptr,
			/* m_size */ sizeof(struct module_state),
			/* m_methods */ mdtw_methods,
			/* m_slots */ nullptr,
			/* m_traverse */ mdtw_traverse,
			/* m_clear */ mdtw_clear,
			/* m_free */ reinterpret_cast<freefunc>(mdtw_free)
	};

	namespace match {

		static const char *const get_path_docstring = nullptr; // TODO docstring

		PyObject *get_path(DTWMatch *self) {
			if (self->pypaths == nullptr) {
				auto paths = new PyObject *[self->nvars];

				for (size_t i = 0; i < self->nvars; i++) {
					path_node *node = self->path[i];
					size_t j = 0;
					while (node != nullptr) {
						j++;
						node = node->next;
					}

					PyObject *pypath = PyList_New(j);
					node = self->path[i];
					j = 0;
					while (node != nullptr) {
						PyList_SET_ITEM(pypath, j++, Py_BuildValue("(ii)", node->x, node->y));
						node = node->next;
					}
					paths[i] = pypath;
				}
				if (self->nvars == 1)
					self->pypaths = paths[0];
				else {
					self->pypaths = PyList_New(self->nvars);
					for (size_t i = 0; i < self->nvars; i++)
						PyList_SetItem(self->pypaths, i, paths[i]);
				}
			}
			return Py_INCREF(self->pypaths), self->pypaths;
		}

		static const char *const align_series_docstring = nullptr; // TODO docstring

		PyObject *align_series(DTWMatch *self, PyObject *args) {

			PyObject *xarg = nullptr;
			if (!PyArg_ParseTuple(args, "O", &xarg))
				return nullptr;
			CHECK_OR_RAISE(PyArray_Check(xarg), "Argument for align_series must be a numpy array");
			auto *xarray = reinterpret_cast<PyArrayObject *>(xarg);
			CHECK_OR_RAISE(
					(PyArray_NDIM(xarray) == 1 && self->nvars == 1) ||
					PyArray_DIM(xarray, 0) == (npy_intp) self->nvars,
					"input array for align_series has a different number of variables from the DTWMatch object "
					"(%d and %lu)", PyArray_DIM(xarray, 0), self->nvars)
			if (PyArray_DESCR(xarray)->type_num != COST_MAT_TYPENUM) {
				CHECK_OR_RAISE(
						PyArray_CanCastArrayTo(xarray, PyArray_DescrFromType(COST_MAT_TYPENUM), NPY_SAFE_CASTING),
						"Input arrays be of a dtype that is safely castable to %s",
						PyArray_DescrFromType(COST_MAT_TYPENUM)->typeobj->tp_name);
				xarray = reinterpret_cast<PyArrayObject *>(PyArray_Cast(xarray, COST_MAT_TYPENUM));
			}

			auto path = self->path[0];
			while (path->next != nullptr)
				path = path->next;

			auto xdata = reinterpret_cast<costmat_item_t *>(PyArray_DATA(xarray));
			npy_intp aligned_size = path->y + 1,
					dimensions[2] = {(npy_intp) self->nvars, aligned_size};

			auto aligned_series = reinterpret_cast<PyArrayObject *>(
					PyArray_ZEROS(PyArray_NDIM(xarray),
					              (PyArray_NDIM(xarray) > 1) ? dimensions : dimensions + 1,
					              COST_MAT_TYPENUM, 0));
			auto aligned_data = reinterpret_cast<costmat_item_t *>(PyArray_DATA(aligned_series));
			auto aligned_count = new long[aligned_size * self->nvars];
			auto vstride = PyArray_NDIM(xarray) > 1 ? PyArray_STRIDE(xarray, 0) / sizeof(costmat_item_t) : 1,
					tstride = PyArray_NDIM(xarray) > 1 ? PyArray_STRIDE(xarray, 1) / sizeof(costmat_item_t) : 1;
			memset(aligned_count, 0, sizeof(long) * aligned_size * self->nvars);

			for (size_t i = 0; i < self->nvars; i++) {
				path = self->path[i];
				while (path != nullptr) {
					aligned_data[i * aligned_size + path->y] += xdata[i * vstride + path->x * tstride];
					aligned_count[i * aligned_size + path->y]++;
					path = path->next;
				}
			}

			for (long i = 0; i < aligned_size * (long) self->nvars; i++) {
				if (aligned_count[i] == 0) {
					PyErr_SetObject(PyExc_TypeError,
					                PyUnicode_FromFormat("Aligned series has missing value(s) (at i=%d)", i));
					return nullptr;
				}
				aligned_data[i] /= aligned_count[i];
			}

			delete[] aligned_count;
			return reinterpret_cast<PyObject *>(aligned_series);
		}

		static const char *const get_total_cost_docstring = nullptr; // TODO docstring

		PyObject *get_total_cost(DTWMatch *self) {
			if (PyObject_TypeCheck(self->costs, &PyList_Type)) {
				double cost = 0;
				PyObject *iterator = PyObject_GetIter(self->costs), *item;
				if (iterator == nullptr)
					return nullptr;

				while ((item = PyIter_Next(iterator))) {
					cost += PyFloat_AsDouble(item);
				}

				return PyFloat_FromDouble(cost);
			}

			return Py_INCREF(self->costs), self->costs;
		}

/* DTWMatch python-specific stuff */

		static PyMemberDef DTWMatch_members[] = {
				{const_cast<char *>("costs"),        T_OBJECT_EX, offsetof(DTWMatch, costs),  0, const_cast<char *>("match cost per variable")},
				{const_cast<char *>("match_length"), T_OBJECT_EX, offsetof(DTWMatch, length), 0, const_cast<char *>("match length")},
				{nullptr, 0, 0,                                           0, nullptr}  /* Sentinel */
		};

		static PyMethodDef DTWMatch_methods[] = {
				{const_cast<char *>("get_path"),       (PyCFunction) get_path,       METH_NOARGS,  get_path_docstring},
				{const_cast<char *>("align_series"),   (PyCFunction) align_series,   METH_VARARGS, align_series_docstring},
				{const_cast<char *>("get_total_cost"), (PyCFunction) get_total_cost, METH_NOARGS,  get_total_cost_docstring},
				{nullptr,                              nullptr, 0,                                 nullptr}
		};

		static int DTWMatch_traverse(DTWMatch *self, visitproc visit, void *arg) {
			Py_VISIT(self->costs);
			Py_VISIT(self->pypaths);
			Py_VISIT(self->length);
			return 0;
		}

		static int DTWMatch_clear(DTWMatch *self) {
			Py_CLEAR(self->costs);
			Py_CLEAR(self->pypaths);
			return 0;
		}

		PyObject *DTWMatch_new(PyTypeObject *type, PyObject *args, PyObject *kwargs) {
			auto *self = (DTWMatch *) type->tp_alloc(type, 0);
			if (self != nullptr) {
				self->pypaths = nullptr;
				Py_INCREF(Py_None);
				self->costs = Py_None;
			}
			return (PyObject *) self;
		}

		void DTWMatch_dealloc(DTWMatch *self) {
			PyObject_GC_UnTrack(self);
			DTWMatch_clear(self);
			for (size_t i = 0; i < self->nvars; i++) {
				while (self->path[i] != nullptr) {
					path_node *next = self->path[i]->next;
					delete self->path[i];
					self->path[i] = next;
				}
			}
			delete[] self->path;
			Py_TYPE(self)->tp_free((PyObject *) self);
		}


		PyTypeObject DTWMatchType =
				pyutils::init_pytypeobject("dtw.DTWMatch",
				                           sizeof(DTWMatch),
				                           0,
				                           Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_GC,
				                           [](PyTypeObject &tp) {
					                           tp.tp_doc = "DTW Match";
					                           tp.tp_new = (newfunc) DTWMatch_new;
					                           tp.tp_dealloc = (destructor) DTWMatch_dealloc;
					                           tp.tp_traverse = (traverseproc) DTWMatch_traverse;
					                           tp.tp_clear = (inquiry) DTWMatch_clear;
					                           tp.tp_members = DTWMatch_members;
					                           tp.tp_methods = DTWMatch_methods;
				                           });
	}
}

/* Module initialization  */
PyMODINIT_FUNC PyInit_mdtw() {
	import_array();

	if (PyType_Ready(&mdtw::match::DTWMatchType) < 0)
		return nullptr;

	PyObject *module = PyModule_Create(&mdtw::moduledef);
	if (module == nullptr)
		return nullptr;

	struct mdtw::module_state *st = mdtw::get_state(module);
	st->error = PyErr_NewException("mdtw_dtw.Error", nullptr, nullptr);
	if (st->error == nullptr) {
		Py_DECREF(module);
		return nullptr;
	}

	st->dtype = PyArray_DescrFromType(mdtw::COST_MAT_TYPENUM);
	Py_INCREF(st->dtype);

	st->preinit_array = nullptr;
	st->preinit_array_len = 0;
	st->last_width = 0;
	st->last_height = 0;
	st->last_nvars = 1;

	Py_INCREF(&mdtw::match::DTWMatchType);
	PyModule_AddObject(module, "DTWMatch", reinterpret_cast<PyObject *>(&mdtw::match::DTWMatchType));
	Py_INCREF(st->dtype);
	PyModule_AddObject(module, "dtype", reinterpret_cast<PyObject *>(st->dtype));

	return module;
}