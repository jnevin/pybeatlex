#ifndef MDTW_CPP_CPPPYUTILS_HPP
#define MDTW_CPP_CPPPYUTILS_HPP

#include "Python.h"

namespace pyutils {

	inline PyTypeObject init_pytypeobject(const char *tp_name,
	                                      Py_ssize_t tp_basicsize,
	                                      Py_ssize_t tp_itemsize,
	                                      unsigned long tp_flags,
	                                      void (*func)(PyTypeObject &)) noexcept {
		PyTypeObject result = {};
		result.ob_base = {PyObject_HEAD_INIT(nullptr) 0};
		result.tp_name = tp_name;
		result.tp_basicsize = tp_basicsize;
		result.tp_itemsize = tp_itemsize;
		result.tp_itemsize = tp_itemsize;
		result.tp_flags = tp_flags;
		func(result);
		return result;
	}
}

#endif //MDTW_CPP_CPPPYUTILS_HPP
