#include <sstream>
#include <iostream>
#include "dtw.hpp"
#include "cpppyutils.hpp"
#include "structmember.h"

// Translate i, j coordinates to the inline index in the full cost matrix
#define FULLMAT_INLINE(i, j) (((i) + 1) * (h+1) + ((j) + 1))
// Transpose the inlined index of the cost matrix by (+1, +1), as needed in dtw(...)
//#define MAT_TRANS(i) ((i) + h + (i)/h + 2)
#define CHECK_OR_RAISE(cond, ...) if (!(cond)) {\
    PyErr_SetObject(PyExc_AttributeError, PyUnicode_FromFormat(__VA_ARGS__));\
    return nullptr;\
}
#define SNPRINTF(varname, length, format, ...) char varname[length];\
    PyOS_snprintf(varname, length, format, __VA_ARGS__)

using namespace pyutils;

namespace mdtw {

	inline costmat_item_t _fmin3(costmat_item_t a, costmat_item_t b, costmat_item_t c) {
		costmat_item_t min = a < b ? a : b;
		return min < c ? min : c;
	}

	void _dtw(costmat_item_t *matrix, costmat_item_t *xdat, costmat_item_t *ydat, size_t xstride, size_t ystride, long w, long h,
		          long min_match, long bwidth, costmat_item_t warp_cost_x, costmat_item_t warp_cost_y) {
		matrix[0] = 0;
		// Fill the cost matrix with infinite's
		for (long i = 1; i < (w + 1) * (h + 1); i++)
			matrix[i] = COST_MAT_INF;

		// Fill the matrix with pairwise distance
		// TODO: check performance difference between pre-filling distance or doing it in the main loop
//		for (long i = 0; i < size; i++)
//			matrix[MAT_TRANS(i)] = pow((xdat[i / h]) - (ydat[i % h]), 2);

		if (bwidth > 0 && bwidth < w && bwidth < h) {
			// Sakoe-Chiba band factors. Note that the possibility of a match smaller than len(x) means
			// that the upper and lower bounds must not be parallel if min_match is set
			double sc_inf_factor = (double) h / w,
					sc_sup_factor = min_match == 0 ? sc_inf_factor :
					                (min_match > bwidth ? (double) h / min_match : INFINITY);

			for (long i = 0; i < w; i++) {
				auto start = (long) fmax(0, sc_inf_factor * (i - bwidth/2.) - bwidth/2.),
						stop = (long) fmin(sc_sup_factor * (i + bwidth/2.) + bwidth/2. + 1, h);
				if (start > stop) {
					PyErr_SetString(PyExc_AssertionError, "Unknown error in Sakoe-chiba band boundaries (start>stop)");
					return;
				}
				for (long j = start; j < stop; j++) {
					matrix[FULLMAT_INLINE(i, j)] = pow(xdat[i * xstride] - ydat[j * ystride], 2) +
					                           _fmin3(matrix[FULLMAT_INLINE(i - 1, j - 1)],
					                                  matrix[FULLMAT_INLINE(i - 1, j)] + warp_cost_x,
					                                  matrix[FULLMAT_INLINE(i, j - 1)] + warp_cost_y);
				}
			}
		} else {
			for (long i = 0; i < w; i++)
				for (long j = 0; j < h; j++)
					matrix[FULLMAT_INLINE(i, j)] = pow(xdat[i * xstride] - ydat[j * ystride], 2) +
					                           _fmin3(matrix[FULLMAT_INLINE(i - 1, j - 1)],
					                                  matrix[FULLMAT_INLINE(i - 1, j)] + warp_cost_x,
					                                  matrix[FULLMAT_INLINE(i, j - 1)] + warp_cost_y);
		}
	}

	path_node *_dtw_backtrace(costmat_item_t *matrix, long h, long match_len,
	                          costmat_item_t warp_cost_x,
	                          costmat_item_t warp_cost_y) { /* Build DTW path via backtrace */
		enum {
			DIAG = 0, WARP_X = 1, WARP_Y = 2
		};
		auto *path = new path_node; // Initialize the path's linked list

		long i = match_len - 1, j = h - 1;
		*path = {(size_t) i, (size_t) j, nullptr};

		while (i > 0 || j > 0) {
			costmat_item_t costs[3] = {matrix[FULLMAT_INLINE(i - 1, j - 1)],
			                           matrix[FULLMAT_INLINE(i - 1, j)] + warp_cost_x,
			                           matrix[FULLMAT_INLINE(i, j - 1)] + warp_cost_y};
			int dir = (costs[WARP_X] < costs[DIAG]) ? WARP_X : DIAG;
			if (costs[WARP_Y] < costs[dir]) dir = WARP_Y;

			switch (dir) {
				case DIAG:
					i--;
					j--;
					break;
				case WARP_X:
					i--;
					break;
				case WARP_Y:
					j--;
					break;
				default:
					PyErr_SetString(PyExc_AssertionError,
					                "Unknown error occurred during traceback (unknown dir value)");
					return nullptr;
			}
			if (i < 0 || j < 0) {
				SNPRINTF(msg, 256, "Unknown error occurred during traceback: i (%ld) or j (%ld) negative", i, j);
				PyErr_SetString(PyExc_AssertionError, msg);
				return nullptr;
			}

			// Insert (x,y) at the head of the linked list
			path_node new_node = {(size_t) i, (size_t) j, path};
			path = new path_node;
			*path = new_node;
		}

		return path;
	}
}